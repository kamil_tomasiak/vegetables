package core
{
	import flash.display.Sprite;
	
	import starling.core.Starling;
	
	[SWF(width=700, height=600, frameRate=60, backgroundColor = 0x003366)]
	public class Vegetables extends Sprite
	{
		public function Vegetables()
		{
			var star:Starling = new Starling(Game, stage);
			star.showStats = true;
			star.start();
		}
	}
}
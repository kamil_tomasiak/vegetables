package core
{
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Assets
	{
		[Embed(source="assets/backgroundMenu.jpg")]
		private static var backgroundMenu:Class;
		public static var backgroundMenuTexture:Texture;
		
		[Embed(source="assets/backgroundPlay.jpg")]
		private static var backgroundPlay:Class;
		public static var backgroundPlayTexture:Texture;
		
		[Embed(source="assets/backgroundEndOfLevel.jpg")]
		private static var backgroundEndOfLevel:Class;
		public static var backgroundEndOfLevelTexture:Texture;
		
		[Embed(source="assets/atlas.png")]
		private static var atlas:Class;
		
		public static var texture:TextureAtlas;
		
		[Embed(source="assets/atlas.xml", mimeType="application/octet-stream")]
		private static var atlasXML:Class;
		
		
		public static function init():void
		{
			backgroundMenuTexture = Texture.fromBitmap(new backgroundMenu());
			
			backgroundPlayTexture = Texture.fromBitmap(new backgroundPlay());
			
			backgroundEndOfLevelTexture = Texture.fromBitmap(new backgroundEndOfLevel());
			
			texture = new TextureAtlas(Texture.fromBitmap(new atlas()),
				XML(new atlasXML()));
		}
	}
}
package states
{
	import Interfaces.IState;
	
	import core.Game;
	import starling.events.Event;
	import starling.core.Starling
	
	import starling.display.Sprite;
	import objects.BackgroundMenu;
	import starling.animation.Tween;
	import starling.text.TextField;
	
	public class GameOver extends Sprite implements IState
	{
		private var game:Game;
		private var background:BackgroundMenu;
		
		public function GameOver(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{	
			background = new BackgroundMenu();
			addChild(background);
			background.alpha = 0.2;
			
			var showEnd:Tween = new Tween(background, 3);
			//wieksza alphe do 1
			showEnd.fadeTo(1);
			Starling.juggler.add(showEnd);
			
			var gameOverText : TextField = new TextField(200, 100, "Koniec", "KomikaAxis", 52, 0x951032);
			gameOverText.x = 250;
			gameOverText.y = 250;
			addChild(gameOverText);
		}
		
		public function destroy():void
		{
			
		}
	}
}
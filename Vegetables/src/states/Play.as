package states
{
	import Interfaces.IState;
	
	import core.Game;
	
	import managers.OperationManager;
	import managers.VegetablesManager;
	
	import objects.BackgroundPlay;
	
	import services.PhpService;
	
	import starling.display.Sprite;
	
	public class Play extends Sprite implements IState
	{
		private var background:BackgroundPlay;
		public var game:Game;
		public var vegetablesFromServer:Array;
		public var operationManager:OperationManager;
		public var vegetablesManager:VegetablesManager;
		
		public function Play(game: Game)
		{
			PhpService.getTableOfVegetablesRequest(this);
			
			this.game = game;
		}
		
		private function init():void
		{
			background = new BackgroundPlay();
			addChild(background);
			
			operationManager = new OperationManager(this);
			
			vegetablesManager = new VegetablesManager(this);
		}
		
		public function getTableOfVegetablesResponse(vegetables:Array):void
		{
			vegetablesFromServer = vegetables;
			
			init();
		}
		
		public function destroy():void
		{
			
		}
	}
}
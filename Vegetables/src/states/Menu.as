package states
{
	import Interfaces.IState;
	
	import core.Assets;
	import core.Game;
	
	import objects.BackgroundMenu;
	
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Menu extends Sprite implements IState
	{
		private var background:BackgroundMenu;
		private var game:Game;
		private var play:Button;
		
		
		public function Menu(game :Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{	
			background = new BackgroundMenu();
			addChild(background);
			
			play = new Button(Assets.texture.getTexture("buttonPlay"));
			play.addEventListener(Event.TRIGGERED, playGame);
			play.pivotX = play.width * 0.5;
			play.pivotY = play.width * 0.5;
			play.x = 350;
			play.y = 300;
			addChild(play);
			
		}
		
		private function playGame(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		public function destroy():void
		{
			
		}
	}
}
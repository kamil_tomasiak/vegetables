package services
{
	import flash.net.NetConnection;
	import flash.net.Responder;

	public class PhpService
	{
		private static var connection:NetConnection;
		private static var responder:Responder;
		
		/**
		 * Zainicjowanie polaczenia z serwerem
		 */
		public static function init():void
		{
			connection = new NetConnection();
			connection.connect("http://strona2.home.pl/TESTY/amfphp-2.2/Amfphp/index.php");
		}//http://strona2.home.pl/TESTY/amfphp-2.2/Amfphp/index.php
		//http://localhost:83/amfphp-2.2/Amfphp/index.php
		
		/**
		 * Odczytywanie danych z serwera
		 * wywolanie metody getTableOfVegetables po stronie serwera
		 */
		public static function getTableOfVegetablesRequest(delegate:Object):void
		{
			responder = new Responder(delegate.getTableOfVegetablesResponse, null);
			connection.call("VegetablesGameService/getTableOfVegetables", responder);
		}
	}
}
package objects
{
	import core.Assets;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class BackgroundMenu extends Sprite
	{
		public function BackgroundMenu()
		{
			var img: Image = new Image(Assets.backgroundMenuTexture);
			img.blendMode = BlendMode.NONE;
			addChild(img);
		}
	}
}
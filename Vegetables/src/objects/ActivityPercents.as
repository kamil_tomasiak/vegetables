package objects
{
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class ActivityPercents extends Sprite
	{
		private var percentText:TextField;
		private var amountOfPercent :int =0;
		
		public function ActivityPercents()
		{
			percentText = new TextField(150, 100, amountOfPercent.toString() + " %", "KomikaAxis", 32, 0xFFFFFF);
			percentText.hAlign "right";
			percentText.y = 200;
			addChild(percentText);
		}
		
		/**
		 * Odswierzenie ilosci procent
		 */
		public function refreshPercent(amount: int):void
		{
			amountOfPercent = amount;
			
			percentText.text = amountOfPercent.toString() + " %";
		}
		
	}
}
package objects
{
	import core.Assets;
	
	import managers.VegetablesManager;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	import states.Play;

	
	/**
	 * 
	 * stratY : pozycja poczatkowa gdzie powinien sie ustawic
	 * 
	 * tableX : ktory wiersz w tabeli
	 * tableY : ktora kolumna w tabeli
	 */
	public class Vegetable
	{
		private var play:Play;
		public var vegetableImage: VegetableImage;
		public var background: VegetableGround;
		
		public var tableX :int;
		public var tableY :int;
		private var vegetablesManager: VegetablesManager;
		public var numberVegetable:int;
		
		public var mainNumberVegetable:int;
		
		public var wasChecking :Boolean = false;
		
		public function Vegetable(play:Play, vegetablesManager:VegetablesManager, tableX:int, tableY:int)
		{
			this.play = play;
			
			//tableY - to kolumna
			this.tableX = tableX;
			this.tableY = tableY;
			
			this.vegetablesManager = vegetablesManager;
		}
		
		
		
		/**
		 * Klikam w owoc i wtedy to sie wykonuje
		 */
		public function selectingFruit(event:TouchEvent):void
		{
			var touches:Vector.<Touch> = event.getTouches(background);
			
			if (touches.length == 1)
			{
				var touch:Touch = touches[0];   
				if (touch.phase == TouchPhase.ENDED)
				{
					vegetablesManager.clickInVegetable(this);
				}
			}
		}
		
		/**
		 * Ustawienie tla
		 */
		public function setBackground(positionX : int, positionY: int):void
		{
			background = new VegetableGround();
			background.addEventListener(starling.events.TouchEvent.TOUCH, selectingFruit);
			background.x = positionX;
			background.y = positionY;
			
			play.addChild(background);
		}
		
		/**
		 * Ustawienie obrazka dla owocu
		 */
		public function setFruitImage(numberVegetable:int, positionX: int):void
		{
			vegetableImage = new VegetableImage(numberVegetable);
			vegetableImage.touchable = false;
			vegetableImage.x = positionX;
			vegetableImage.y = -50;
			this.numberVegetable = numberVegetable;

			//ustawienie koloru glownego, warzywa
			setMainVegetableNumber(numberVegetable);
			
			play.addChild(vegetableImage);
		}
		
		public function getPositionX():int
		{
			return vegetableImage.x;
		}
		
		public function getPositionY():int
		{
			return vegetableImage.y;
		}
		
		public function setPositionY(value : int):void
		{
			vegetableImage.y = value;
		}
		
		/**
		 * Zmiana obecnego warzywa na inne nowe, potrzebne do zapobiegania blokowaniu
		 */
		public function changeVegetableImageTo(number : int):void
		{			
			numberVegetable = number;
			
			setMainVegetableNumber(number);
				
			var removeVegetableAnimation:Tween = new Tween(vegetableImage, 0.3);
			//zmniejsza alphe do 0
			removeVegetableAnimation.fadeTo(0);
			Starling.juggler.add(removeVegetableAnimation);
			
			vegetableImage.changeImage(number);
			vegetableImage.alpha = 0;
			
			var showVegetable:Tween = new Tween(vegetableImage, 0.3);
			//wieksza alphe do 1
			showVegetable.fadeTo(1);
			Starling.juggler.add(showVegetable);
		}
		
		public function changeVegetableImageToNull():void
		{
			if(numberVegetable !=0)
			{
				vegetablesManager.increaseAmountOfAnimation();
				
				//TODO zapelnianie zapelnienia planszy
				//zebralem dany element wiec musze to zaktualizowac po lewej
				//vegetablesManager.additivesManager.refreshAdditionForFruit(numberVegetable);
				
				numberVegetable = 0;
				this.mainNumberVegetable = 0;
				
				var removeVegetableAnimation:Tween = new Tween(vegetableImage, 0.15);
				//zmniejsza alphe do 0
				removeVegetableAnimation.fadeTo(0);
				removeVegetableAnimation.onComplete = endAnimation;
				Starling.juggler.add(removeVegetableAnimation);
			}
		}
		
		//TODO
		private function endAnimation():void
		{
			vegetablesManager.decreaseAmountOfAnimation();
			
			if (vegetablesManager.getAmountOfAnimation() == 0)
			{
				//jezeli usuwanie juz zakonczone
				//if(vegetablesManager.thrashingManager.didRemovingEnd())
				//{
					vegetablesManager.fallingManager.startFallingForVegetables();
				//}
			}
		}
		
		/**
		 * Ustawienie koloru glownego dla podanego numeru warzywa
		 */
		private function setMainVegetableNumber(numberVegetable :int):void
		{
			//wybor odpowiedniego koloru, koloru podstawowego
			if((numberVegetable >=1 && numberVegetable <=5) || (numberVegetable >=11 && numberVegetable <=12))
			{
				mainNumberVegetable = numberVegetable;	
			}
			else if(numberVegetable >=6 && numberVegetable <=10)
			{
				mainNumberVegetable = numberVegetable - 5;
			}
			else if(numberVegetable >=13 && numberVegetable <=15)
			{
				mainNumberVegetable = 1;
			}
			else if(numberVegetable >=16 && numberVegetable <=18)
			{
				mainNumberVegetable = 2;
			}
			else if(numberVegetable >=19 && numberVegetable <=21)
			{
				mainNumberVegetable = 3;
			}
			else if(numberVegetable >=22 && numberVegetable <=24)
			{
				mainNumberVegetable = 4;
			}
			else if(numberVegetable >=25 && numberVegetable <=27)
			{
				mainNumberVegetable = 5;
			}
		}
		
		/**
		 * Warzywo zostalo sprawdzone
		 */
		public function setWasChecking():void
		{
			wasChecking = true;
		}
		
		/**
		 * Warzywo nie zostalo sprawdzone
		 */
		public function unSetWasChecking():void
		{
			wasChecking = false;
		}
		
		/**
		 * Sprawdzam czy warzywo bralo udzial w sprawdzaniu
		 */
		public function getValueWasChecking():Boolean
		{
			return wasChecking;
		}
		
	}
}
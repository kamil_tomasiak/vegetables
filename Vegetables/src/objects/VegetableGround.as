package objects
{
	import core.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class VegetableGround extends Sprite
	{
		private var img:Image;
		
		public function VegetableGround()
		{
			img = new Image(Assets.texture.getTexture("fruitGround"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
		/*
		public function changeImageToFrame():void
		{
			img.removeFromParent(true);
			img = new Image(Assets.texture.getTexture("frame"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
		
		public function changeImageToGround():void
		{
			img.removeFromParent(true);
			img = new Image(Assets.texture.getTexture("fruitGround"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
		
		/**
		 * Obrazek pomocniczego zaznaczenia
		 
		public function changeImageToHint():void
		{
			img.removeFromParent(true);
			img = new Image(Assets.texture.getTexture("fruitGroundCheck"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
		*/
	}
}
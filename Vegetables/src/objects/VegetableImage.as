package objects
{
	import core.Assets;
	import core.Game;
	
	import starling.animation.Juggler;
	import starling.animation.Tween;
	import starling.display.Image;
	import starling.display.Sprite;

	
	/**
	 * 
	 * numberFruit : numer obrazka
	 */
	public class VegetableImage extends Sprite
	{
		private var numberVegetable:int;
		private var img:Image;
		
		public function VegetableImage(numberImg :int)
		{
			//za pierwszym razem nie trzeba nic usuwac dlatego stworzona metoda setImage
			//ktora rozni sie tylko od changeImage removeFromParent
			setImage(numberImg);
		}
		
		public function changeImage(toNumber: int):void
		{
			img.removeFromParent(true);
			
			setImage(toNumber);
			
		}
		
		private function setImage(number: int):void
		{
			switch(number)
			{
				case 1:
					img = new Image(Assets.texture.getTexture("veg1"));
					break;
				case 2:
					img = new Image(Assets.texture.getTexture("veg2"));
					break;
				case 3:
					img = new Image(Assets.texture.getTexture("veg3"));
					break;
				case 4:
					img = new Image(Assets.texture.getTexture("veg4"));
					break;
				case 5:
					img = new Image(Assets.texture.getTexture("veg5"));
					break;
				case 6:
					img = new Image(Assets.texture.getTexture("veg1All"));
					break;
				case 7:
					img = new Image(Assets.texture.getTexture("veg2All"));
					break;
				case 8:
					img = new Image(Assets.texture.getTexture("veg3All"));
					break;
				case 9:
					img = new Image(Assets.texture.getTexture("veg4All"));
					break;
				case 10:
					img = new Image(Assets.texture.getTexture("veg5All"));
					break;
				case 11:
					img = new Image(Assets.texture.getTexture("superVertical"));
					break;
				case 12:
					img = new Image(Assets.texture.getTexture("superHorizontal"));
					break;
				case 13:
					img = new Image(Assets.texture.getTexture("veg12x"));
					break;
				case 14:
					img = new Image(Assets.texture.getTexture("veg13x"));
					break;
				case 15:
					img = new Image(Assets.texture.getTexture("veg14x"));
					break;
				case 16:
					img = new Image(Assets.texture.getTexture("veg22x"));
					break;
				case 17:
					img = new Image(Assets.texture.getTexture("veg23x"));
					break;
				case 18:
					img = new Image(Assets.texture.getTexture("veg24x"));
					break;
				case 19:
					img = new Image(Assets.texture.getTexture("veg32x"));
					break;
				case 20:
					img = new Image(Assets.texture.getTexture("veg33x"));
					break;
				case 21:
					img = new Image(Assets.texture.getTexture("veg34x"));
					break;
				case 22:
					img = new Image(Assets.texture.getTexture("veg42x"));
					break;
				case 23:
					img = new Image(Assets.texture.getTexture("veg43x"));
					break;
				case 24:
					img = new Image(Assets.texture.getTexture("veg44x"));
					break;
				case 25:
					img = new Image(Assets.texture.getTexture("veg52x"));
					break;
				case 26:
					img = new Image(Assets.texture.getTexture("veg53x"));
					break;
				case 27:
					img = new Image(Assets.texture.getTexture("veg54x"));
					break;
				case 28:
					img = new Image(Assets.texture.getTexture("superTime"));
					break;
			}
			
			numberVegetable = number;
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
		
		
	}
}
package objects
{
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class AmountOfPoints extends Sprite
	{
		private var pointsText:TextField;
		private var amountOfPoints : int =0;
		//ilosc punktow za jedno warzywo
		private var amountPointsForOneVegetable : int = 10;
		
		public function AmountOfPoints()
		{
			pointsText = new TextField(150, 100, amountOfPoints.toString(), "KomikaAxis", 25, 0xFFFFFF);
			pointsText.hAlign "left";
			pointsText.y = 300;
			addChild(pointsText);
		}
		
		/**
		 * Dodaje punkty
		 */
		public function addPoints(count: int):void 
		{
			amountOfPoints = amountOfPoints + (count * amountPointsForOneVegetable);
			
			pointsText.text = amountOfPoints.toString();
		}
		
	}
}
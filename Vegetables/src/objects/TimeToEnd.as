package objects
{
	import managers.ActivityManager;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import states.GameOver;
	
	public class TimeToEnd extends Sprite
	{
		private var timeText:TextField;
		private var seconds:int = 120;
		private var activityManager :ActivityManager;
		
		public function TimeToEnd(activityManager : ActivityManager)
		{
			this.activityManager = activityManager;
			
			timeText = new TextField(150, 100, seconds.toString(), "KomikaAxis", 32, 0xFFFFFF);
			timeText.hAlign "right";
			addChild(timeText);
		}
		
		/**
		 * Ilosc czasu zmniejsza sie o jedna sekunde
		 * odpowiendnie wyswietlenie na ekranie
		 */
		public function tickTime():void
		{
			seconds--;
			
			if( seconds > 0)
			{
				timeText.text = seconds.toString();	
			}
			else if(seconds == 0)
			{
				timeText.text = seconds.toString();	
				
				//koniec gry
				activityManager.gameOver();
			}
			
		}
		
		/**
		 * Ustawienie aktualnej ilosci sekund
		 */
		public function addSecond(seconds:int):void
		{
			this.seconds = this.seconds +  seconds;	
			timeText.text = this.seconds.toString();
		}
	}
}
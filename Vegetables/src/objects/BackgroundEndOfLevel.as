package objects
{
	import core.Assets;
	
	import starling.display.BlendMode;
	import starling.display.Sprite;
	import starling.display.Image;
	
	public class BackgroundEndOfLevel extends Sprite
	{
		public function BackgroundEndOfLevel()
		{
			var img: Image = new Image(Assets.backgroundEndOfLevelTexture);
			img.blendMode = BlendMode.NONE;
			addChild(img);
		}
	}
}
package objects
{
	import core.Assets;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class BackgroundPlay extends Sprite
	{
		public function BackgroundPlay()
		{
			var img: Image = new Image(Assets.backgroundPlayTexture);
			img.blendMode = BlendMode.NONE;
			addChild(img);
		}
	}
}
package managers
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import objects.AmountOfPoints;
	import objects.Vegetable;
	import objects.VegetableImage;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.events.Event;
	
	import states.Play;

	public class VegetablesManager
	{
		private var vegetablesAreBusy : Boolean  = true;
		private var amountOfAnimations: int = 0;
		public var play:Play;
		
		public var vegetables:Array;
		private var vegetablesFromServer:Array;
		
		private var numberSetOfVegetables : int = 0; //numer zestawu owocow
		private var timerFalling:Timer;
		public var thrashingManager:ThrashingManager;
		public var fallingManager:FallingManager;
		private var blockingTableManager:BlockingTableManager;
		public var activityManager:ActivityManager;
		
		public function VegetablesManager(play: Play)
		{
			this.play = play;
			
			//tablica warzyw
			vegetables = new Array();
			
			//obsluga usuwania warzyw
			thrashingManager = new ThrashingManager(this);
			
			//manager odpowiedzialny za spadanie warzyw
			fallingManager = new FallingManager(this);
			
			//utworzenie obiektow fruit
			createTableFruits();
			
			//ustawienie owocow na miejsca
			setStartPositionForVegetables();
			
			//timer dla poczatkowego ladowania obrazkow
			timerFalling = new Timer(230);
			timerFalling.addEventListener(TimerEvent.TIMER ,prepareVegetableTable);
			timerFalling.start(); 
			
			//obiekt odpowiedzialny za zabezpieczanie przed blokowaniem 
			blockingTableManager = new BlockingTableManager(this);
			
			//klasa odpowiedzialna za nagradzanie aktywnosci
			activityManager = new ActivityManager(this);

		}
		
		/**
		 * Przygotowuje tablice owocow (tla i owocu) ktore zostana wyswietlone - ustawia pozycje
		 */
		private function createTableFruits():void
		{
			var x:int;
			var y:int = 550;
			var i:int;
			var j:int;
			var tmpFruit:Array;
			var vegetable :Vegetable;
			
			//ladowanie tla dla owocow lewy dolny rog element 0,0
			for(i=0; i<9; i++)
			{
				tmpFruit = new Array();
				x = 150; //bylo 70
				y -= 54; //bylo 54
				for(j=0; j<9; j++)
				{
					x += 54;
					vegetable = new Vegetable(play, this, j, i);
					vegetable.setBackground(x, y);
					tmpFruit.push(vegetable);
				}
				vegetables.push(tmpFruit);
			}
		}
		
		/**
		 * Pobieram kolejny numer warzywa z serwera i zapisuje dla obiektów Vegetable tworzacych tabele rozgrywki.
		 */
		private function setStartPositionForVegetables():void
		{
			vegetablesAreBusy = true;
			
			vegetablesFromServer = play.vegetablesFromServer;
			var vegetable : Vegetable;
			for(var i:int=0; i<9; i++)
			{
				for(var j:int=0; j<9; j++)
				{
					vegetable = vegetables[i][j];
					vegetable.setFruitImage(vegetablesFromServer[i + numberSetOfVegetables*9][j], 150 + (j+1)*54); //zamiast 120 bylo 70, i 64->54
				}
			}
			
			numberSetOfVegetables++;
		}
		
		/**
		 * Metoda laduje na samym starcie wszyskie owoce uruchamia spadanie dla kolejnych...
		 */
		public function prepareVegetableTable(event:TimerEvent):void
		{ 
			var i:int = timerFalling.currentCount -1;
			
			if (i<9)
			{	
				vegetablesAreBusy = true;
				
				for(var j:int=0; j<9; j++)
				{
					var vegetable : Vegetable = vegetables[i][j];
					var moveVegetable:Tween = new Tween(vegetable.vegetableImage, 1-(i/10));
					moveVegetable.moveTo(vegetable.getPositionX(), 550 - ((i+1)*54));
					moveVegetable.onComplete = decreaseAmountOfAnimation;
					Starling.juggler.add(moveVegetable);
					
					increaseAmountOfAnimation();
				}
			}
			else
			{
				timerFalling.reset();
				timerFalling.removeEventListener(TimerEvent.TIMER, prepareVegetableTable);
				//juz mozna zaczynac klikac
				vegetablesAreBusy = false;
				
				//start czasu rozgrywki
				activityManager.startGameTimer();
			}
		}
		
		/**
		 * Stworzenie nowego warzywa, na odpowiednią kolumną
		 * parametry : wiersz / kolumna
		 */
		public function createNewVegetableFor(i:int, j :int):Vegetable
		{
			var newVegetable : Vegetable;
			newVegetable = new Vegetable(play, this, j, i);
			newVegetable.setFruitImage(vegetablesFromServer[i][j], 150 + (j+1)*54);
			
			return newVegetable;
		}
		
		/**
		 * Zamiana dwoch warzyw
		 * -> zamiana warzyw przypisanych do odpowiedniego miejsca w tabeli
		 * -> zamiana numerow warzyw
		 */
		public function changeTwoVegetableInLogic(vegetableFirst: Vegetable, vegetableSecond: Vegetable):void
		{
			//zamiana obrazkow z dwoch wybranych obiektow
			var tmpVegetable : VegetableImage = vegetableFirst.vegetableImage;
			vegetableFirst.vegetableImage = vegetableSecond.vegetableImage;
			vegetableSecond.vegetableImage = tmpVegetable;
			
			//zmiana numeru owocu
			var tmpNumberVegetable:int = vegetableFirst.numberVegetable;
			vegetableFirst.numberVegetable = vegetableSecond.numberVegetable;
			vegetableSecond.numberVegetable = tmpNumberVegetable;
			
			var tmpMainNumberVegetable:int = vegetableFirst.mainNumberVegetable;
			vegetableFirst.mainNumberVegetable = vegetableSecond.mainNumberVegetable;
			vegetableSecond.mainNumberVegetable = tmpMainNumberVegetable;
		}
		
		/**
		 * Metoda jest wywolywana gdy zostanie klikniete jakies warzywo
		 */
		public function clickInVegetable(vegetable : Vegetable):void
		{
			if(!didVegetablesAreBusy())
			{
				thrashingManager.removeVegetablesAfterClick(vegetable);
			}
		}
		
		/**
		 * Sprawdzenie czy jakies owoce cos robia
		 */
		public function didVegetablesAreBusy():Boolean
		{
			if(vegetablesAreBusy || amountOfAnimations > 0)
			{
				return true;
			}
			
			return false;
		}
		
		/**
		 * Cos jest wykonywane na warzywach - sa zajete
		 */
		public function setVegetablesAreBusy():void
		{
			vegetablesAreBusy = true;
		}
		
		/**
		 * Warzywa nie sa zajete
		 */
		public function unSetVegetablesAreBusy():void
		{
			vegetablesAreBusy = false;
		}
		
		/**
		 * Zwiekszenie ilosci animacji ktore sie wykonuja
		 */
		public function increaseAmountOfAnimation():void
		{
			amountOfAnimations++;
		}
			
		/**
		 * Zmniejszenie ilosci animacji 
		 */
		public function decreaseAmountOfAnimation():void
		{
			amountOfAnimations--;
		}
		
		/**
		 * Zwraca ilosc animacji ktore obecnie sa wykonywane
		 */
		public function getAmountOfAnimation():int
		{
			return amountOfAnimations;
		}
		
		/**
		 * Zapobieganie przed blokowaniem
		 */
		public function preventionBlocking():void
		{
			//zapobieganie blokowaniu, wykrywanie elementu ktory trzeba zamienic 
			//aby plansza sie nie zablokowala
			blockingTableManager.checkDidChangeSameVegetable();	
		}
	}
}
package managers
{
	import core.Game;
	
	import flash.events.Event;

	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	import states.Play;

	public class OperationManager
	{
		private var timerWaitForEndOperation:Timer;
		
		private var tableSavingOperation : Array;
		//private var fruitsManager:FruitsManager;
		private var play:Play;
		
		public function OperationManager(play : Play)
		{
			this.play = play;
			
			tableSavingOperation = new Array();
			
			timerWaitForEndOperation = new Timer(125);
			timerWaitForEndOperation.addEventListener(TimerEvent.TIMER, makeSavingOperation);
			timerWaitForEndOperation.start();
		}
		
		/**
		 * Gdy wszytskie animacje sa zakonczone timer wykonuje odpowiednia operacje
		 */
		protected function makeSavingOperation(event:Event):void
		{
			//jezeli na planszy nic sie teraz nie dzieje
			if (tableSavingOperation.length >0 && !play.vegetablesManager.didVegetablesAreBusy())
			{
				switch(tableSavingOperation[0])
				{
					//koniec gry
					case 1:
						play.game.changeState(Game.GAME_OVER_STATE);
						break; 
				}
				
				tableSavingOperation.splice(0, 1);
			}
		}
		
		/**
		 * Dodanie operacji gameOver do kolejki
		 */
		public function doOperationChangeStateToGameOver():void
		{
			tableSavingOperation.push(1);
		}
	}
}
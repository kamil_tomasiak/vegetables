package managers
{   
	import objects.Vegetable;

	/**
	 * Klasa odpowiedzialna za nieblokowanie planszy
	 */
	public class BlockingTableManager
	{
		private var vegetables:Array;
		private var vegetablesManager:VegetablesManager;
		
		//grupy warzyw
		private var localVegetable:Array;
		//tablica zawierajaca grupy warzyw
		private var groupsVegetable:Array;
		
		
		public function BlockingTableManager(vegetablesManager: VegetablesManager)
		{		
			this.vegetablesManager = vegetablesManager;
			this.vegetables = vegetablesManager.vegetables;
			
		}
		
		/**
		 * Sprawdzam czy zamienic jakies warzywo, jesli jest
		 * za malo grup to zamieniam, funkcja dostepna z zewnatrz.
		 */
		public function checkDidChangeSameVegetable():void
		{
			vegetablesManager.setVegetablesAreBusy();
			
			changeAllVegetablesForNotChecking();
			
			//sprawdzam czy istnieje wystarczajaca ilosc grup do usuniecia
			//jezeli nie istnieje to wchodze do if
			if(!checkDidExistVegetableToRemove())
			{
				//odznaczam wszytskie
				changeAllVegetablesForNotChecking();
				
				//teraz zaznacze te ktorych nie powinienem uzywac czyli
				//te ktore tworza zestawy wieksze niz '2', tworza tzw. '3'
				//robie to po to zebym ich nie mogl wybrac 
				for(var l:int = groupsVegetable.length-1; l>=0; l--)
				{
					for(var z:int = groupsVegetable[l].length-1; z>=0; z--)
					{
						//zaznaczam te elementy
						groupsVegetable[l][z].setWasChecking();			
					}
				}
				
				//wybieram element do zamiany i go zamieniam
				chooseVegetableToChange();
			}
			
			changeAllVegetablesForNotChecking();
			
			vegetablesManager.unSetVegetablesAreBusy();
		}
		
		/**
		 * Wybieram warzywo ktore nalezy zmienic
		 */
		private function chooseVegetableToChange():Boolean
		{
			for(var i:int= 0; i<=8; i++)
			{
				for(var j:int= 0; j<=8; j++)
				{
					var vegetable : Vegetable = vegetables[i][j];
					
					//nie jest zaznaczony (nie nalezy do grupy jakiejs '3')
					if(!(vegetable.getValueWasChecking()))
					{	
						//sprawdzam 3 mozliwych sasiadow
						if((i+1)<=8 && didVegetableAreTheSame(vegetable, vegetables[i+1][j]))
						{							
							//teraz trzeba wybrac do zmiany
							if((i+2)<=8 && didVegetableIsProper(vegetables[i+2][j]))
							{
								vegetables[i+2][j].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
							else if((j+1)<=8 && didVegetableIsProper(vegetables[i+1][j+1]))
							{
								vegetables[i+1][j+1].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
							else if((j+1)<=8 && didVegetableIsProper(vegetables[i][j+1]))
							{
								vegetables[i][j+1].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
						}
						else if((i+1)<=8 && (j+1)<=8 && didVegetableAreTheSame(vegetable, vegetables[i+1][j+1]))
						{
							//teraz trzeba wybrac do zmiany
							if(didVegetableIsProper(vegetables[i+1][j]))
							{
								vegetables[i+1][j].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
							else if(didVegetableIsProper(vegetables[i][j+1]))
							{
								vegetables[i][j+1].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
						}
						else if((j+1)<=8 && didVegetableAreTheSame(vegetable, vegetables[i][j+1]))
						{
							//teraz trzeba wybrac do zmiany
							if((j+2)<=8 && didVegetableIsProper(vegetables[i][j+2]))
							{
								vegetables[i][j+2].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
							else if((i+1)<=8 && didVegetableIsProper(vegetables[i+1][j]))
							{
								vegetables[i+1][j].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
							else if((i+1)<=8 && didVegetableIsProper(vegetables[i+1][j+1]))
							{
								vegetables[i+1][j+1].changeVegetableImageTo(vegetable.mainNumberVegetable);
								return true;
							}
						}
					}
				}
			}	
			return false;
		}
		
		/**
		 * Czy warzywo nie bylo juz wybierane i czy jest to podstawowe warzywo a nie jakis super przedmiot
		 */
		private function didVegetableIsProper(vegetable: Vegetable):Boolean
		{
			if(vegetable.numberVegetable <=5 && !vegetable.getValueWasChecking())
			{
				return true;
			}
			return false;
		}
		
		private function didVegetableAreTheSame(vegetable: Vegetable, neighborVegetable: Vegetable):Boolean
		{
			if(!neighborVegetable.getValueWasChecking() && vegetable.mainNumberVegetable == neighborVegetable.mainNumberVegetable)
			{
				return true;
			}
			
			return false;
		}
		
		/**
		 * Sprawdzam czy sa conajmniej 2 grupy do usuwania
		 */
		private function checkDidExistVegetableToRemove():Boolean
		{
			groupsVegetable = new Array();
			
			for(var i:int= 0; i<=8; i++)
			{
				for(var j:int= 0; j<=8; j++)
				{
					if(!vegetables[i][j].getValueWasChecking())
					{
						localVegetable = new Array();
						
						checkVegetable(vegetables[i][j]);
						
						if(localVegetable.length >2)
						{
							groupsVegetable.push(localVegetable);
							
							//jezeli jest wiecej niz 2 to przerywamy
							if(groupsVegetable.length > 2)
							{
								return true;
							}
						}
					}
				}
			}
			
			return false;
		}
		
		/**
		 * Sprawdza czy poszczegolne warzywo jest do usuniecia
		 */
		private function checkVegetable(vegetable: Vegetable):void
		{
			//zaznaczam ze to warzywo bylo juz sprawdzane
			vegetable.setWasChecking();
			localVegetable.push(vegetable);
			
			var x: int = vegetable.tableX;
			var y: int = vegetable.tableY;
			
			if(x<=7)
			{
				checkDidItIsToRemove(vegetable, vegetables[y][x+1]);
			}
			
			if(x>=1)
			{
				checkDidItIsToRemove(vegetable, vegetables[y][x-1]);
			}
			
			if(y<=7)
			{
				checkDidItIsToRemove(vegetable, vegetables[y+1][x]);
			}
			
			if(y>=1)
			{
				checkDidItIsToRemove(vegetable, vegetables[y-1][x]);
			}	
		}
		
		/**
		 * Pomaga podczas usuwania warzyw sprawdza czy podane warzywo nalezy usunac
		 * jesli tak to uruchamia dla niego usuwanie
		 */
		private function checkDidItIsToRemove(vegetable : Vegetable, vegetableNeighbor : Vegetable):void
		{
			if(!vegetableNeighbor.getValueWasChecking() && vegetable.mainNumberVegetable == vegetableNeighbor.mainNumberVegetable)
			{
				checkVegetable(vegetableNeighbor);
			}
		}
		
		/**
		 * Odznaczenie wszytskich warzyw jako nie odczytywane
		 */
		private function changeAllVegetablesForNotChecking():void
		{
			//odznaczam wszystkie elementy tablicy, jako nie sprawdzane
			for(var i:int= 0; i<=8; i++)
			{
				for(var j:int= 0; j<=8; j++)
				{
					vegetables[i][j].unSetWasChecking();
				}
			}
		}
		
	}
}
package managers
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import objects.Vegetable;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	
	
	public class FallingManager
	{
		private var vegetableManager:VegetablesManager;
		private var thrashingManager:ThrashingManager;
		private var vegetables:Array;
		
		private var timerFalling : Timer;
		
		private var indexNewVegetables : Array; 
		//ilosc usunietych w kazdej kolumnie
		private var amountRemoveInEveryColumn:Array;
		
		public function FallingManager(vegetableManager: VegetablesManager)
		{
			this.vegetableManager = vegetableManager;
			this.thrashingManager = vegetableManager.thrashingManager;
			this.vegetables = vegetableManager.vegetables;
			
			indexNewVegetables = [9, 9, 9, 9, 9, 9, 9, 9, 9];
			
		}
		
		/**
		 * Ustawienie timera ktory pozwoli warzywom spadac
		 */
		public function startFallingForVegetables():void
		{	
			vegetableManager.increaseAmountOfAnimation();
			
			timerFalling = new Timer(93, 1);//9 - thrashingManager.getMinY()); //155
			timerFalling.addEventListener(TimerEvent.TIMER ,fallingForVegetables2);
			timerFalling.addEventListener(TimerEvent.TIMER_COMPLETE, endFall);
			timerFalling.start();
		}
		
		protected function endFall(event:TimerEvent):void
		{
			vegetableManager.decreaseAmountOfAnimation();
		}
		
		/**
		 * funkcja wywolywana przez timer
		 * zarzadza spadaniem owocow
		 */
		public function fallingForVegetables(event:TimerEvent):void
		{
			//doczytywanie na razie nie ma pozniej bedzie dla 9
			var i:int = timerFalling.currentCount -1 + thrashingManager.getMinY();
			var y: int;
			
			var len: int = thrashingManager.getMaxX();
			for(var j: int=thrashingManager.getMinX(); j<=len; j++)
			{
				//jezeli ma cos dla niego spasc z gory
				if(vegetables[i][j].numberVegetable == 0)
				{
					var vegetableFirst :Vegetable = vegetables[i][j];
					var vegetableSecond :Vegetable = null;
					
					
					y = i;
					while (y<9 && vegetables[y][j].numberVegetable == 0)
					{
						y++;
					}
					
					//jezeli jest to ktorys z widocznych
					if( y < 9)
					{
						vegetableSecond = vegetables[y][j];
					}//musi byc stworzony nowy
					else
					{
						vegetableSecond = vegetableManager.createNewVegetableFor(getNextIndexForVegetable(j), j);
					}
					
					vegetableManager.changeTwoVegetableInLogic(vegetableFirst, vegetableSecond);
					
					
					//w ostatnim rzucie
					if(timerFalling.currentCount == 9 - thrashingManager.getMinY() && j==len)
					{
						//zapobieganie blokowaniu
						vegetableManager.preventionBlocking();
					}
						
					//zamiana graficznie zapamietanie pozycji i przesuniecie
					var tmpPosX: int = vegetableFirst.getPositionX();
					var tmpPosY: int = vegetableFirst.getPositionY();
					
					//vegetableManager.increaseAmountOfAnimation();
					var moveVegetable:Tween = new Tween(vegetableFirst.vegetableImage, 0.09); //bylo 0.15
					moveVegetable.moveTo(vegetableSecond.getPositionX(), vegetableSecond.getPositionY());
					//moveVegetable.onComplete = vegetableManager.decreaseAmountOfAnimation;
					Starling.juggler.add(moveVegetable);
					
					//vegetableManager.increaseAmountOfAnimation();
					var moveVegetable2:Tween = new Tween(vegetableSecond.vegetableImage, 0.09);
					moveVegetable2.moveTo(tmpPosX, tmpPosY);
					//moveVegetable2.onComplete = vegetableManager.decreaseAmountOfAnimation;
					Starling.juggler.add(moveVegetable2);
				}
			}
			
		}
		
		/**
		 * Zwraca indeks kolejnego warzywa
		 */
		private function getNextIndexForVegetable(column: uint): uint
		{
			var newIndex: uint = indexNewVegetables[column];
			
			indexNewVegetables[column]++;
			
			if (indexNewVegetables[column] == 100)
			{
				indexNewVegetables[column] = 0;
			}
			
			return newIndex;
		}
		
		/**
		 * Nowa wersja spadania
		 */
		public function fallingForVegetables2(event:TimerEvent):void
		{
			var minX:int = thrashingManager.getMinX();
			var maxX:int = thrashingManager.getMaxX();
			var minY:int = thrashingManager.getMinY();
			
			//wyzerowanie tablicy zawierajacej ilosc elementow usunietych w danej kolumnie
			amountRemoveInEveryColumn = [[0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0],
										 [0, 0, 0, 0, 0, 0, 0, 0, 0]];
			
			var i:int;
			var j:int;
			
			
			// Przygotowanie tablicy z iloscia elemtow do usuniecia
			for( j = minX; j<= maxX; j++)
			{
				for( i = minY; i<= 8; i++)
				{
					var veTmp : Vegetable = vegetables[i][j];
					
					//ten element jest elementem usunietym
					if(veTmp.numberVegetable == 0)
					{
						if(i-1 >= 0)
						{
							amountRemoveInEveryColumn[i][j] = amountRemoveInEveryColumn[i-1][j] + 1;
						}
						else
						{
							amountRemoveInEveryColumn[i][j] = 1;
						}
					}
					else // element ktory nie byl usuniety
					{
						if(i-1 >= 0)
						{
							amountRemoveInEveryColumn[i][j] = amountRemoveInEveryColumn[i-1][j];
						}
					}
				}
				//ilosc elementow w danej kolumnie zostaje zapisana w ostatnim wierszu tablicy
				amountRemoveInEveryColumn[9][j] = amountRemoveInEveryColumn[8][j];
			}
			
			for( j = minX; j<= maxX; j++)
			{
				for( i = minY; i<= 8; i++)
				{
					var vege: Vegetable = vegetables[i][j];
					var y: int;
					var vegeSecond : Vegetable;
					
					if(vege.numberVegetable == 0)
					{		
						y = i+1;
						while(y<9 && vegetables[y][j].numberVegetable == 0)
						{
							y++;
						}
						
						//jezeli jest to ktorys z widocznych
						if( y < 9)
						{
							vegeSecond = vegetables[y][j];
							
							//zmiana w tableli
							var tmp:int = amountRemoveInEveryColumn[i][j];
							amountRemoveInEveryColumn[i][j] = amountRemoveInEveryColumn[y][j];
							amountRemoveInEveryColumn[y][j] = tmp;
							
						}//musi byc stworzony nowy
						else
						{
							vegeSecond = vegetableManager.createNewVegetableFor(getNextIndexForVegetable(j), j);
						}
						
						vegetableManager.changeTwoVegetableInLogic(vege, vegeSecond);
					}
				}
			}
			
			//ustawienie tych na samej gorze w odpowiednie miejsca
			for(j = minX; j<=maxX; j++)
			{
				var which : int = 2;
				for(i = 9-amountRemoveInEveryColumn[9][j]; i<=8; i++)
				{
					var vegTm : Vegetable = vegetables[i][j];
					vegTm.setPositionY(-54 * which);
					which++;
				}
			}
			
			//zapobieganie blokowaniu
			vegetableManager.preventionBlocking();
					
			//spadanie graficzne
			for(i = minY; i<=8; i++)
			{
				for(j = minX; j<=maxX; j++)
				{
					var vegeTmp : Vegetable = vegetables[i][j];
					
					if(vegeTmp.getPositionY() > -50)
					{
						var moveVegetable:Tween = new Tween(vegeTmp.vegetableImage, 0.09); //bylo 0.15
						moveVegetable.moveTo(vegeTmp.getPositionX(), vegeTmp.getPositionY() + ( (amountRemoveInEveryColumn[i][j])*54 ));
						//moveVegetable.onComplete = vegetableManager.decreaseAmountOfAnimation;
						Starling.juggler.add(moveVegetable);
					
					} //jezeli nie to sa to te na samej goze
					else
					{
						var moveVegetable1:Tween = new Tween(vegeTmp.vegetableImage, 0.09); //bylo 0.15
						moveVegetable1.moveTo(vegeTmp.getPositionX(), vegeTmp.getPositionY() + ( (amountRemoveInEveryColumn[9][j])*54 ) + 118);
						//moveVegetable.onComplete = vegetableManager.decreaseAmountOfAnimation;
						Starling.juggler.add(moveVegetable1);
					}
				}
			}
			
		}
		
	}
}
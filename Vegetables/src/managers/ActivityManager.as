package managers
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import objects.ActivityPercents;
	import objects.AmountOfPoints;
	import objects.TimeToEnd;
	
	import states.Play;
	
	/**
	 * Klasa zarzadzajaca, ladowaniem dodatkowej miarki
	 */
	public class ActivityManager
	{
		//ilosc procent zapelnionych
		private var amountOfPercent : int =0;
		
		//ilosc sekund nic nie robienia
		private var amountOfFreeSeconds : int =0;
		
		private var timer:Timer;
		private var vegetablesManager:VegetablesManager;
		private var timeToEnd:TimeToEnd;
		private var activityPercents:ActivityPercents;
		private var timerPercent:Timer;
		private var timerPower:Timer;
		private var amountSecondForPower : int;
		//czy super moc jest uzywana
		private var usingSuperPower : Boolean = false;
		private var amountOfPoints:AmountOfPoints;
		
		public function ActivityManager(vegetablesManager : VegetablesManager)
		{
			this.vegetablesManager = vegetablesManager;
			
			timer = new Timer(1000);
			timer.addEventListener(TimerEvent.TIMER, timeGo);
			
			timeToEnd = new TimeToEnd(this);
			vegetablesManager.play.addChild(timeToEnd);
			
			timerPercent = new Timer(500);
			timerPercent.addEventListener(TimerEvent.TIMER, checkDidSomethingIsRemoving);
			timerPercent.start();
			
			activityPercents = new ActivityPercents();
			vegetablesManager.play.addChild(activityPercents);
			
			amountOfPoints = new AmountOfPoints();
			vegetablesManager.play.addChild(amountOfPoints);
		}
		
		/**
		 * Rozpoczynam odliczanie czasu
		 */
		public function startGameTimer():void
		{
			timer.start();
		}
		
		/**
		 * Czas plynie dalej
		 */
		protected function timeGo(event:TimerEvent):void
		{
			//czas plynie
			timeToEnd.tickTime();
		}
		
		/**
		 * Sprawdzanie co sekunde czy cos sie dzieje
		 */
		protected function checkDidSomethingIsRemoving(event:TimerEvent):void
		{	
			//jezeli jest zajety to oznacza ze odbywa sie spadanie || lub moze super moc jest uzywana
			if(!vegetablesManager.didVegetablesAreBusy() && !usingSuperPower)
			{
				amountOfFreeSeconds++;
			}
				
			//gdy za dlugo nic sie nie dzieje to zmniejszam ilosc procent
			if(amountOfFreeSeconds >= 3)
			{
				decreaseAmountOfPercentFor( 2 * (amountOfFreeSeconds - 2));
			}
		}
		
		
		/**
		 * Dodawanie procentow za usuwanie elementow z planszy
		 */
		public function addPercentForRemove(amountRemoveElements: int):void
		{
			amountOfFreeSeconds = 0;
			
			increaseAmountOfPercentFor(amountRemoveElements * 2);
		}
		
		/**
		 * Zwiekszenie ilosci procent o
		 */
		private function increaseAmountOfPercentFor(count:int):void
		{
			amountOfPercent = amountOfPercent + count;
			
			if(amountOfPercent >= 100)
			{
				amountOfPercent = 100;
				
				//uruchomienie timera super mocy
				startUsingSuperPower();
			}
			
			//odswierzenie widoku procent
			activityPercents.refreshPercent(amountOfPercent);
		}
		
		/**
		 * Zmniejszenie ilosci procent 
		 */
		public function decreaseAmountOfPercentFor(count:int):void
		{
			amountOfPercent = amountOfPercent - count;
			
			if(amountOfPercent  < 0)
			{
				amountOfPercent = 0;
			}
			
			//odswierzenie widoku procent
			activityPercents.refreshPercent(amountOfPercent);
		}
		
		/**
		 * Kara za zle klikniecie
		 * jesli masz super moc to czas super mocy zostanie zmniejszony
		 * jesli jest normalny tryb to zostanie odjete 20 procent
		 */
		public function punishmentForBadClick():void
		{
			//jezeli super moc jest uzywana
			if(usingSuperPower)
			{
				//zmniejszenie ilosci sekund - ile ma sie wykonywac super power
				amountSecondForPower = amountSecondForPower - 2;	
			}
			
			decreaseAmountOfPercentFor(20);
		}
		
		//blok metod odpowiedzialnych za to co sie dzieje gdy super moc napelnie sie do 100%
		
		/**
		 * Zwraca wartosc true, false w zaleznosci od tego czy super moc jest uzywana czy nie
		 */
		public function didSuperPowerIsUsing():Boolean
		{
			return usingSuperPower;	
		}
		
		/**
		 * Rozpoczecie uzywania super mocy
		 */
		private function startUsingSuperPower():void
		{
			usingSuperPower = true;
			
			timerPower = new Timer(1000, 10);
			amountSecondForPower = 10;
			timerPower.addEventListener(TimerEvent.TIMER, usingPower);
			timerPower.addEventListener(TimerEvent.TIMER_COMPLETE, endSuperPower);
			timerPower.start();
		}
		
		/**
		 * Wywoluje sie co sekunde gdy dysponujemy super sila (5razy)
		 */
		protected function usingPower(event:TimerEvent):void
		{
			
			if(amountSecondForPower >=1)
			{
				decreaseAmountOfPercentFor(10);
				amountSecondForPower--;
			}
			else
			{
				usingSuperPower = false;	
				timerPower.reset();
			}
		}
		
		/**
		 * Koniec uzywania super mocy
		 */
		protected function endSuperPower(event:TimerEvent):void
		{
			usingSuperPower = false;	
			timerPower.reset();
		}
		// koniec metod odpowiedzialnych za super moc
		
		/**
		 * Funkcja wykozystywana aby dodac dodatkowy czas
		 * parametr to ilosc czasu jaki dodajemy
		 */
		public function addExtraTime(amount : int):void
		{
			timeToEnd.addSecond(amount);			
		}
		
		/**
		 * Dodanie puktow za usuniecie warzyw
		 * Metoda wywolywana w ThrashingManager po usunieciu warzyw
		 */
		public function givePointAfterRemoveVegetables(amountOfVegetables: int, multiplier: int):void
		{
			amountOfPoints.addPoints(amountOfVegetables * multiplier);
		}
		
		
		/**
		 * Ustawienie konca gry - game over
		 * wywolywana metoda przez timeToEnd gdy czas sie skonczyl
		 */
		public function gameOver():void
		{
			//wstawienie odpowedniej operacji do kolejki
			vegetablesManager.play.operationManager.doOperationChangeStateToGameOver();
		}
		
	}
}
package managers
{
	import objects.Vegetable;
	
	import states.Play;

	public class ThrashingManager
	{
		public var vegetablesToRemove : Array;
		private var vegetablesManager:VegetablesManager;
		public var vegetables:Array;
		
		private var minY: int = 8;
		private var maxY: int = 0;
		private var minX: int = 8;
		private var maxX: int = 0;
		
		private var multiplier :int;
		
		public function ThrashingManager(vegetablesManager : VegetablesManager)
		{
			vegetablesToRemove = new Array();
			
			this.vegetablesManager = vegetablesManager;
			
			vegetables = vegetablesManager.vegetables;
		}
		
		/**
		 * Usuwanie owocow po kliknieciu
		 */
		public function removeVegetablesAfterClick(vegetable : Vegetable):void
		{
			vegetablesManager.setVegetablesAreBusy();
			
			vegetablesToRemove = new Array();
			multiplier = 0;
			
			var amountOfRemovingElements :int =0;
			
			//ustawienie wartosci granicznych | tzw. wyzerowanie min max , X,Y
			minY = vegetable.tableY;
			maxY = vegetable.tableY;
			minX = vegetable.tableX;
			maxX = vegetable.tableX; 
			var i:int;
			var j:int;
			var vegetableNeighbor : Vegetable;
			
			//normalne do zbicia i super punktowane
			if(vegetable.numberVegetable <= 5 || (vegetable.numberVegetable >12 && vegetable.numberVegetable<=27) )
			{
				//wybieram warzywa do usuniecia
				chooseVegetableToRemoveWith(vegetable);
				
				amountOfRemovingElements = vegetablesToRemove.length;
				
				//dodawanie procent za usuniecie elementow z planszy
				if(amountOfRemovingElements > 2)
				{
					if(vegetablesManager.activityManager.didSuperPowerIsUsing())
					{
						//usuwanie z planszy z super moca
						removeElementsFromTableWith(true);
					}
					else
					{
						//usuwanie z planszy w normalnym trybie
						removeElementsFromTableWith(false);
						//doliczanie procentow za zbijanie
						vegetablesManager.activityManager.addPercentForRemove(amountOfRemovingElements);
					}
				}
				else
				{
					//odznaczanie
					for (i=vegetablesToRemove.length-1; i>=0; i--)
					{
						vegetableNeighbor = vegetablesToRemove[i];
						vegetableNeighbor.unSetWasChecking();
					}
					
					//zmniejszenie ilosci procent za niewlasciwe klikniecie
					vegetablesManager.activityManager.punishmentForBadClick();
				}
				
			}   //super usuwajacy wszystkie tego samego typu
			else if((vegetable.numberVegetable > 5) && (vegetable.numberVegetable <= 10))
			{
				//wybranie tych do usuwania
				for(i=0; i<=8; i++)
				{
					for(j=0; j<=8; j++)
					{
						vegetableNeighbor = vegetables[i][j];
						
						if(vegetable.mainNumberVegetable == vegetableNeighbor.mainNumberVegetable)
						{
							vegetablesToRemove.push(vegetableNeighbor);
							//ustawienie obszary do usuwania Min Max, X,Y
							setValueMinMaxFor(vegetableNeighbor);
						}
					}
				}
				
				//usuwanie z planszy w normalnym trybie
				removeElementsFromTableWith(false);
				
				//nie dodaje procent ale nie bedzie tez karcony za nic nie robienie
				vegetablesManager.activityManager.addPercentForRemove(0);
				
				amountOfRemovingElements = vegetablesToRemove.length;

			}	//super niszczyciel pionowy   
			else if(vegetable.numberVegetable == 11) 
			{
				minY = 0;
				maxY = 8;
				minX = vegetable.tableX;
				maxX = vegetable.tableX;
				
				for(i=0; i<=8; i++)
				{
					vegetables[i][vegetable.tableX].changeVegetableImageToNull();					
				}
				
				amountOfRemovingElements = 9;
				
				//nie dodaje procent ale nie bedzie tez karcony za nic nie tobienie
				vegetablesManager.activityManager.addPercentForRemove(0);
				
			}   //super niszczyciel poziomy
			else if(vegetable.numberVegetable == 12)
			{
				maxY = vegetable.tableY;
				minY = vegetable.tableY;
				minX = 0;
				maxX = 8;
				
				for(j=0; j<=8; j++)
				{
					vegetables[vegetable.tableY][j].changeVegetableImageToNull();
				}

				amountOfRemovingElements = 9;
				
				//nie dodaje procent ale nie bedzie tez karcony za nic nie tobienie
				vegetablesManager.activityManager.addPercentForRemove(0);
			}
			else if(vegetable.numberVegetable == 28)
			{
				vegetable.changeVegetableImageToNull();
				
				//dodatkowy czas
				vegetablesManager.activityManager.addExtraTime(20);
				
				amountOfRemovingElements = 1;
				
				//nie dodaje procent ale nie bedzie tez karcony za nic nie robienie
				vegetablesManager.activityManager.addPercentForRemove(0);
			}
			
			//jezeli jest cos zbite
			if(amountOfRemovingElements >2)
			{
				if(multiplier == 0)
				{
					multiplier =1;
				}
				
				//naliczanie punktow za zbijanie
				vegetablesManager.activityManager.givePointAfterRemoveVegetables(amountOfRemovingElements, multiplier);
			}
			
			vegetablesManager.unSetVegetablesAreBusy();
		}
		
		
		/**
		 * Usuwanie elementow z tablicy z odpowiednim trybem
		 * parametr: true - super usuwanie
		 * parametr: false - zwykle usuwanie
		 */
		private function removeElementsFromTableWith(type: Boolean):void
		{
			var i:int;
			var veget : Vegetable;
			
			/**
			 * Super usuwanie wraz z sąsiadami
			 */
			if(type)
			{
				for (i=vegetablesToRemove.length-1; i>=0; i--)
				{
					veget = vegetablesToRemove[i];
					
					//jezeli to jest mnoznik to
					if (veget.numberVegetable >=13 && veget.numberVegetable <=27)
					{
						//zapisuje wartosc mnoznika
						multiplier = multiplier +  (veget.numberVegetable - (((veget.mainNumberVegetable-1)*3) + 11));
					}
					
					veget.unSetWasChecking();
					veget.changeVegetableImageToNull();
					
					removeOneElementNeighbors(veget);
				}
			}
			else //usuwanie zwykle, tylko elementow z tablicy
			{
				//usuwam
				for (i=vegetablesToRemove.length-1; i>=0; i--)
				{
					veget = vegetablesToRemove[i];
					
					//jezeli to jest mnoznik to
					if (veget.numberVegetable >=13 && veget.numberVegetable <=27)
					{
						//zapisuje wartosc mnoznika
						multiplier = multiplier +  (veget.numberVegetable - (((veget.mainNumberVegetable-1)*3) + 11));
					}
					
					veget.unSetWasChecking();
					veget.changeVegetableImageToNull();
				}
			}	
		}
		
		/**
		 * Funckja pomocnicza dla removeElementsFromTable
		 */
		private function removeOneElementNeighbors(veg : Vegetable):void
		{
			var x:int = veg.tableX;
			var y:int = veg.tableY;
			var vegTmp : Vegetable;
			
			if(x+1<=8)
			{
				vegTmp = vegetables[y][x+1];
				vegTmp.changeVegetableImageToNull();
				vegTmp.unSetWasChecking();
				setValueMinMaxFor(vegTmp);
			}
			if(x-1>=0)
			{
				vegTmp = vegetables[y][x-1];
				vegTmp.changeVegetableImageToNull();
				vegTmp.unSetWasChecking();
				setValueMinMaxFor(vegTmp);
			}
			if(y-1>=0)
			{
				vegTmp = vegetables[y-1][x];
				vegTmp.changeVegetableImageToNull();
				vegTmp.unSetWasChecking();
				setValueMinMaxFor(vegTmp);
			}
			if(y+1<=8)
			{
				vegTmp = vegetables[y+1][x];
				vegTmp.changeVegetableImageToNull();
				vegTmp.unSetWasChecking();
				setValueMinMaxFor(vegTmp);
			}
		}
		
		/**
		 * Zapisywanie owocow ktore sa tego samego koloru co klikniety
		 */
		private function chooseVegetableToRemoveWith(vegetable : Vegetable):void
		{	
			//zaznaczam ze to warzywo bylo juz sprawdzane
			vegetable.setWasChecking();
			vegetablesToRemove.push(vegetable);
			
			var x: int = vegetable.tableX;
			var y: int = vegetable.tableY;
			
			//ustawienie wartosci min, max
			setValueMinMaxFor(vegetable);
			
			if(x<=7)
			{
				checkDidItIsToRemove(vegetable, vegetables[y][x+1]);
			}
			
			if(x>=1)
			{
				checkDidItIsToRemove(vegetable, vegetables[y][x-1]);
			}
			
			if(y<=7)
			{
				checkDidItIsToRemove(vegetable, vegetables[y+1][x]);
			}
			
			if(y>=1)
			{
				checkDidItIsToRemove(vegetable, vegetables[y-1][x]);
			}
		}
		
		/**
		 * Ustawienie wartosci min max, X,Y
		 */
		private function setValueMinMaxFor(vegetable: Vegetable):void
		{
			var x: int = vegetable.tableX;
			var y: int = vegetable.tableY;
			
			//wyznaczenie wartosci min max X,Y
			if(x < minX)
			{
				minX = x;
			}
			if(x > maxX)
			{
				maxX = x;
			}
			if(y < minY)
			{
				minY = y;
			}
			if(y > maxY)
			{
				maxY = y;
			}
			//koniec wyboru min, max, X,Y
		}
		
		/**
		 * Pomaga podczas usuwania warzyw sprawdza czy podane warzywo nalezy usunac
		 * jesli tak to uruchamia dla niego usuwanie
		 */
		private function checkDidItIsToRemove(vegetable : Vegetable, vegetableNeighbor : Vegetable):void
		{
			if(!vegetableNeighbor.getValueWasChecking() && vegetable.mainNumberVegetable == vegetableNeighbor.mainNumberVegetable)
			{
				chooseVegetableToRemoveWith(vegetableNeighbor);
			}
		}
		
		/**
		 * Zwraca wartosc minX
		 */
		public function getMinX():int
		{
			return minX;
		}
		
		/**
		 * Zwraca wartosc maxX
		 */
		public function getMaxX():int
		{
			return maxX;
		}
		
		/**
		 * Zwraca wartosc minY
		 */
		public function getMinY():int
		{
			return minY;
		}
		
		/**
		 * Zwraca wartosc maxY
		 */
		public function getMaxY():int
		{
			return maxY;
		}
		
	}
}
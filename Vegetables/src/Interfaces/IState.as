package Interfaces
{
	public interface IState
	{
		function destroy():void;
	}
}